
![Mockup for feature A](https://1.bp.blogspot.com/-D0j6dLmK_eY/XnuXm20KQWI/AAAAAAAAA5M/enhTsf88aNgQatKpOGW3O6UkqFjjPxjRwCLcBGAsYHQ/s1600/5250873.jpeg)

# Introduction

Данная игра является моим тестовым заданием для компании [Beresnev Games]

#

Готовые билды находятся в корне проекта в папке Builds 

## Задание было следущим:

* Реализовать игру "Пинг Понг" в которой можно играть одному (Singleplayer) или вдвоём (Multiplayer).
* В игре должна присутствовать возможность выбора скина рокетки и сохранения лучшего счета, эти данные не должны сбрасываться после перезапуска приложения.
* Игра должна быть адаптирована под мобильные устройства.

## Немного скринов

![Mockup for feature A](https://1.bp.blogspot.com/-g7H3gS5bpCM/XnuXKrmq4UI/AAAAAAAAA44/WzHLjIT5A3I1TnMcZEBj_1kH6AOyhB4kgCLcBGAsYHQ/s1600/Screen01.jpg)

![Mockup for feature A](https://1.bp.blogspot.com/-2dVE8Td--sM/XnuXKkJgUHI/AAAAAAAAA48/Depxdv_1p_wJ9SxkWblw4YwAMTBGPAmdACLcBGAsYHQ/s1600/Screen02.jpg)

![Mockup for feature A](https://1.bp.blogspot.com/-qC4bV95qTkg/XnuXKrGm1WI/AAAAAAAAA5A/ok3sY0WDnoo1zXehlewla82cEuYudWcNQCLcBGAsYHQ/s1600/Screen03.jpg)

# Network

Мультиплеерная часть оказалась самая сложная, её я пытался реализовать тремя различными способами:
#
* Используя WebSocket
* Unity Transport и Unity NetCode
* Photon Bolt

Изначально приложение разрабатывалось под WebSocket и архитектура была построена на паттерне MVC, 
а также в своей архитектуре я решил реализовать так называемые [управляемые обновления]. В итоге получилась следующая архитектура:

![Mockup for feature A](https://1.bp.blogspot.com/-HGAxaNj36YE/Xnu_AOeZ7aI/AAAAAAAAA50/MuoAcjiK9dkvWcjJoAEBGwcHBIZla41gQCLcBGAsYHQ/s1600/Unti2131tled123-11111111111.jpg)

![Mockup for feature A](https://1.bp.blogspot.com/-r56Cy9We7aY/XnuPvx_nKGI/AAAAAAAAA4s/sK2awF-nM-orAlA5_qDSfGmnuPoSyaqowCLcBGAsYHQ/s1600/Untitled-1-Recovered.jpg)

### Основные плюсы такой архитектуры
* Вызов методов Awake, Start, Update происходит в нужном для вас порядке, тогда когда вам это нужно.
* Удобства отладки используя Brakepoints в Visual Studio
* Увеличение производительности

## Unity.Transport Unity.NetCode ESC

Но когда дело дошло до Клиент-серверной части выяснилось, что WebSocket в сыром виде совсем не подходит для реалтайм передачи данных, был большой лаг.
Было решено искать другой вариант реализации нетворка, я решил не изменять традициям и попытался использовать встроенные юнитевские SDK - [Unity.Transport] и [Unity.NetCode].

После трёх дней работы у меня получилась примерно следующая архитектура:

![Mockup for feature A](https://1.bp.blogspot.com/-50WquQMZuec/XnufywDxp9I/AAAAAAAAA5Y/WXeEBAe1BTALbMH2sNG1cTw6gFLFb5MggCLcBGAsYHQ/s1600/Untitled-1.jpg)

Такой бардак получился из-за того что [Unity.NetCode] основан на [ECS] с которым я абсолютно не знаком, как в принципе и с [Unity.Transport] и [Unity.NetCode].

## Photon Bolt

У меня оставался последний вариант - [Photon Bolt].
С ним у меня не возникло ни каких проблем, за исключением того, что пришлось полностью переписать архитектуру приложения и полностью отказаться от управляемых обновлений 
и вернуться к неуправляемым.

## Никаких Public fields и [SerializedField]

В приложении можно увидеть, что все MonoBehaviour скрипты находящиеся в сцене не имеют публичный полей или полей с аттрибутом [SerializedField], 
все поля заполняются только кодом, никакого Drag and Drop, это очень полезно при работе в команде с репозиторием,
не нужно постоянно комитить сцену и решать Merge conflicts, а также это помогает избежать случайной поломки объекта руками других разработчиков.

#

![Mockup for feature A](https://1.bp.blogspot.com/-4M0eebmJ-0g/XnxikS9OnFI/AAAAAAAAA6M/7Aq0NIl8SYQhKlUWf-vab5xNOouc9_IxQCLcBGAsYHQ/s1600/%25D0%259A%25D1%2580%25D0%25B8%25D0%25B2%25D1%258B%25D0%25B5%2B%25D1%2580%25D1%2583%25D0%25BA%25D0%25B8.jpg)

Заполнение полей происходит следующим образом:

```javascript
void Awake()
{
   ButtonSignleplayer    = Utils.Find<Button>("CanvasMainMenuUI/ButtonSignlePlayer");
   ButtonStartServer     = Utils.Find<Button>("CanvasMainMenuUI/ButtonStartServer");
   ButtonConnectToServer = Utils.Find<Button>("CanvasMainMenuUI/ButtonConnectToServer");
   ButtonChooseSkin      = Utils.Find<Button>("CanvasMainMenuUI/ButtonChooseSkin");
   TextBestScore         = Utils.Find<Text>("CanvasMainMenuUI/TextBestScore");

   ButtonNextSkin     = Utils.Find<Button>("CanvasMainMenuUI/ButtonNextSkin");
   ButtonPreviousSkin = Utils.Find<Button>("CanvasMainMenuUI/ButtonPreviousSkin");
   ButtonSaveAndBack  = Utils.Find<Button>("CanvasMainMenuUI/ButtonSaveAndBack");
   SkinPreview        = Utils.Find<GameObject>("CanvasMainMenuUI/SkinPreview");
   ImageSkin          = Utils.Find<Image>("CanvasMainMenuUI/SkinPreview/ImageSkin");

   TextServerCreation     = Utils.Find<Text>("CanvasMainMenuUI/TextServerCreation");
   TextConnectionToServer = Utils.Find<Text>("CanvasMainMenuUI/TextConnectionToServer");
}
```

## Hierarchy

Иерархия объектов в сцене сделана с наименьшей вложенностью, а для большей интуитивности они разделены пустыми объектами на подобии этого: `----------- Scripts -----------`, [так рекомендует сама Юнити].
Это является довольно хорошей практикой в плане стилистики и удобства проекта, так и в плане производительности.

[Одна интересная статейка по иерархии в юнити]


![Mockup for feature A](https://1.bp.blogspot.com/-UohREFCxeoY/XnxcYX9hplI/AAAAAAAAA6A/PoDoEglpthMJJArwcWXbjHcpBnG3IMC-QCLcBGAsYHQ/s1600/2020-03-26_12-38-04.jpg)

## New Unity Input System

Благодаря новой системе ввода Юнити мне удалось без каких либо проблем реализовать мультиплатформенную систему ввода, игра поддерживает все возможные джойстики, клавиатуру и Touchscreen.

![Mockup for feature A](https://1.bp.blogspot.com/-YzyACdxFp4s/Xnx51xAN0YI/AAAAAAAAA6Y/38baqHtjZicbRj6vgpfpZExZXEoLhCBoACLcBGAsYHQ/s1600/20200313_133853.jpg)

## Style cop

Ну и конечно же StyleCop, куда сейчас без него

![Mockup for feature A](https://1.bp.blogspot.com/-RMomvMHe6Nk/Xnx6kb6EvKI/AAAAAAAAA6k/BuH34qw9D5gNDCTdtuDLYTiEhpERiKSyQCLcBGAsYHQ/s640/Use%2BStyleCop.jpg)
                        

[Beresnev Games]: https://beresnev.games/
[Unity.Transport]: https://docs.unity3d.com/Packages/com.unity.transport@0.3/manual/index.html
[Unity.NetCode]: https://docs.unity3d.com/Packages/com.unity.netcode@0.1/manual/index.html
[ECS]: https://docs.unity3d.com/Packages/com.unity.entities@0.8/manual/index.html
[Photon Bolt]: https://assetstore.unity.com/packages/tools/network/photon-bolt-free-127156
[управляемые обновления]: https://blogs.unity3d.com/ru/2015/12/23/1k-update-calls/
[так рекомендует сама Юнити]: https://blogs.unity3d.com/ru/2017/06/29/best-practices-from-the-spotlight-team-optimizing-the-hierarchy/
[Одна интересная статейка по иерархии в юнити]: https://habr.com/ru/post/486636/

