#if ENABLE_VSTU
using System.IO;
using System.Text;
using System.Xml.Linq;
using UnityEditor;

namespace Analyzers
{
    [InitializeOnLoad]
    public class ProjectFileGeneration
    {
        sealed class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding => Encoding.UTF8;
        }

        static ProjectFileGeneration()
        {
            SyntaxTree.VisualStudio.Unity.Bridge.ProjectFilesGenerator.ProjectFileGeneration += (fileName, fileContent) =>
            {
                var xmlDoc = XDocument.Parse(fileContent);
                var projectElement = xmlDoc.Root;
                var projectNamespace = (XNamespace)projectElement.Name.NamespaceName;

                const string basePath = "Assets\\Analyzers";

                var itemGroupElement = new XElement(projectNamespace + "ItemGroup");
                itemGroupElement.Add(new XElement(projectNamespace + "Analyzer", new XAttribute("Include", Path.Combine(basePath, "StyleCop.Analyzers.dll"))));
                itemGroupElement.Add(new XElement(projectNamespace + "Analyzer", new XAttribute("Include", Path.Combine(basePath, "StyleCop.Analyzers.CodeFixes.dll"))));
                projectElement.Add(itemGroupElement);

                var propertyGroupElement = new XElement(projectNamespace + "PropertyGroup");
                propertyGroupElement.Add(new XElement(projectNamespace + "CodeAnalysisRuleSet", Path.Combine(basePath, "Rules.ruleset")));
                propertyGroupElement.Add(new XElement(projectNamespace + "NoWarn", "0649"));
                projectElement.Add(propertyGroupElement);

                using (var xmlString = new Utf8StringWriter())
                {
                    xmlDoc.Save(xmlString);
                    return xmlString.ToString();
                }
            };
        }
    }
}
#endif
