﻿using UnityEngine;

namespace RicochetXtremeRemaster
{
    public class Settings : MonoBehaviour
    {
        void Awake()
        {
            Physics2D.velocityIterations = 50;
            Physics2D.positionIterations = 50;

            Time.fixedDeltaTime = 0.001f;
        }
    }
}