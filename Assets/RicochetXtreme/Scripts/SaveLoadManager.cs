﻿using System.IO;
using UnityEngine;

namespace RicochetXtremeRemaster
{
    public static class SaveLoadManager
    {
        [SerializeField]
        public struct PlayerData
        {
            public int BestScore;
            public int SkinIndex;
        }

        static string FilePath
        {
            get
            {
#if UNITY_STANDALONE_WIN
                return Path.Combine(Application.dataPath, "PlayerData.json");
#elif UNITY_ANDROID
                return Path.Combine(Application.persistentDataPath, "PlayerData.json");
#endif
            }
        }

        public static void SaveBestScore(int currentScore)
        {
            LoadPlayerData(out var playerData);

            if (currentScore > playerData.BestScore)
                playerData.BestScore = currentScore;

            string json = JsonUtility.ToJson(playerData, true);

            File.WriteAllText(FilePath, json);
        }

        public static void SaveSpaceshipSkin(int skinIndex)
        {
            LoadPlayerData(out var playerData);

            playerData.SkinIndex = skinIndex;

            string json = JsonUtility.ToJson(playerData, true);

            File.WriteAllText(FilePath, json);
        }

        public static bool LoadPlayerData(out PlayerData playerData)
        {
            playerData = default;

            if (File.Exists(FilePath))
            {
                var text = File.ReadAllText(FilePath);
                playerData = JsonUtility.FromJson<PlayerData>(text);

                return true;
            }

            return false;
        }
    }
}