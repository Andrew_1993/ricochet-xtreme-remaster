﻿using Bolt;
using System.Text;
using UnityEngine;

namespace RicochetXtremeRemaster
{
    public class MainMenuSceneManager : MonoBehaviour
    {
        StringBuilder stringBuilder; // Anti allocation
        int currentSkinIndex;

        void Start()
        {
            if (SaveLoadManager.LoadPlayerData(out var playerData))
            {
                stringBuilder = new StringBuilder(30);
                stringBuilder.Append("Best score: ");
                stringBuilder.Append(playerData.BestScore.ToString());
                MainMenuSceneUI.Instance.TextBestScore.text = stringBuilder.ToString();

                currentSkinIndex = playerData.SkinIndex;
            }

            MainMenuSceneUI.Instance.ButtonSignleplayer.onClick.AddListener(() => StartSingleplayer());
            MainMenuSceneUI.Instance.ButtonStartServer.onClick.AddListener(() => StartServer());
            MainMenuSceneUI.Instance.ButtonConnectToServer.onClick.AddListener(() => ConnectedToServer());
            MainMenuSceneUI.Instance.ButtonChooseSkin.onClick.AddListener(() => OpenChooseSkinWindow());
            MainMenuSceneUI.Instance.ButtonNextSkin.onClick.AddListener(() => NextSkin());
            MainMenuSceneUI.Instance.ButtonPreviousSkin.onClick.AddListener(() => PreviousSkin());
            MainMenuSceneUI.Instance.ButtonSaveAndBack.onClick.AddListener(() => SaveAndBack());

            MainMenuSceneUI.Instance.View = MainMenuSceneUI.ViewType.MainMenu;

            SoundManager.Instance.PlaySound(soundName: "MainMenuMusic", volume: .3f, loopSound: true, destroyAfterComplete: false);
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                CancelConnecting();
        }

        void StartSingleplayer()
        {
            MainMenuSceneUI.Instance.View = MainMenuSceneUI.ViewType.None;
            BoltLauncher.StartSinglePlayer();
        }

        void StartServer()
        {
            MainMenuSceneUI.Instance.View = MainMenuSceneUI.ViewType.NetworkServerCreation;
            BoltLauncher.StartServer();
            SoundManager.Instance.PlaySound(soundName: "ButtonClick", loopSound: false, destroyAfterComplete: true);
        }

        void ConnectedToServer()
        {
            MainMenuSceneUI.Instance.View = MainMenuSceneUI.ViewType.NetworkConnectionToServer;
            BoltLauncher.StartClient();
            SoundManager.Instance.PlaySound(soundName: "ButtonClick", loopSound: false, destroyAfterComplete: true);
        }

        void CancelConnecting()
        {
            BoltLauncher.Shutdown();

            BoltNetwork.ShutdownImmediate();

            foreach (var connection in BoltNetwork.Connections)
            {
                connection.Disconnect();
            }

            foreach (var entity in BoltNetwork.Entities)
            {
                BoltNetwork.Destroy(entity.gameObject);
            }

            Destroy(FindObjectOfType<ControlBehaviour>().gameObject);

            MainMenuSceneUI.Instance.View = MainMenuSceneUI.ViewType.MainMenu;
        }

        void OpenChooseSkinWindow()
        {
            MainMenuSceneUI.Instance.View = MainMenuSceneUI.ViewType.MenuChooseSkin;
            SpaceshipSkinManager.SetupSkin(MainMenuSceneUI.Instance.ImageSkin, currentSkinIndex);
            SoundManager.Instance.PlaySound(soundName: "ButtonClick", loopSound: false, destroyAfterComplete: true);
        }

        void NextSkin()
        {
            currentSkinIndex++;
            if (currentSkinIndex > SpaceshipSkinManager.SkinCount - 1)
                currentSkinIndex = 0;

            SpaceshipSkinManager.SetupSkin(MainMenuSceneUI.Instance.ImageSkin, currentSkinIndex);
            SoundManager.Instance.PlaySound(soundName: "NextSpaceshipSkin", loopSound: false, destroyAfterComplete: true);
        }

        void PreviousSkin()
        {
            currentSkinIndex--;
            if (currentSkinIndex < 0)
                currentSkinIndex = SpaceshipSkinManager.SkinCount - 1;

            SpaceshipSkinManager.SetupSkin(MainMenuSceneUI.Instance.ImageSkin, currentSkinIndex);
            SoundManager.Instance.PlaySound(soundName: "PreviousSpaceshipSkin", loopSound: false, destroyAfterComplete: true);
        }

        void SaveAndBack()
        {
            MainMenuSceneUI.Instance.View = MainMenuSceneUI.ViewType.MainMenu;
            SaveLoadManager.SaveSpaceshipSkin(currentSkinIndex);
            SoundManager.Instance.PlaySound(soundName: "ButtonClick", loopSound: false, destroyAfterComplete: true);
        }
    }
}