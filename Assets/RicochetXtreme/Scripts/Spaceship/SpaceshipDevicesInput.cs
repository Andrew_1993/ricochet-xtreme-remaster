﻿using UnityEngine;

namespace RicochetXtremeRemaster
{
    public class SpaceshipDevicesInput : Bolt.EntityBehaviour<ISpaceship>
    {
        struct Input
        {
            public float LeftRightArrows;
            public Vector2 LeftRightSticks;
            public bool IsTouched;
            public Vector2 TouchPosition;
        }

        InputControls inputControls;
        Input input;
        Vector2 lastTouchPosition;

        public override void Attached()
        {
            inputControls = new InputControls();
            input = default;

            // Get input from all devices

            inputControls.Keyboard.LeftRightArrows.performed += e => input.LeftRightArrows = e.ReadValue<float>();
            inputControls.Keyboard.LeftRightArrows.canceled  += e => input.LeftRightArrows = 0f;

            inputControls.Gamepad.LeftRightSticks.performed  += e => input.LeftRightSticks = e.ReadValue<Vector2>();
            inputControls.Gamepad.LeftRightSticks.canceled   += e => input.LeftRightSticks = Vector2.zero;

            inputControls.Screen.Touch.started               += TouchStarted;
            inputControls.Screen.Touch.performed             += e => input.IsTouched       = e.ReadValueAsButton();
            inputControls.Screen.Touch.canceled              += TouchCanceled;

            inputControls.Screen.Position.performed          += e => input.TouchPosition   = e.ReadValue<Vector2>();
            inputControls.Screen.Position.canceled           += e => input.TouchPosition   = Vector2.zero;

            inputControls.Enable();
        }

        public override void SimulateOwner()
        {
            float halfWidth      = GameArea.Instance.Scale.x / 2;
            float deltaXPosition = 0f;

            if (input.IsTouched)
            {
                Vector2 touchWorldPos = Camera.main.ScreenToWorldPoint(new Vector3(input.TouchPosition.x, input.TouchPosition.y, 0f));
                var deltaTouch        = touchWorldPos - lastTouchPosition;
                lastTouchPosition     = touchWorldPos;
                deltaXPosition        = deltaTouch.x;
            }
            else
            {
                deltaXPosition = input.LeftRightArrows   != 0f ? input.LeftRightArrows :
                                 input.LeftRightSticks.x != 0f ? input.LeftRightSticks.x : 0f;
                deltaXPosition *= BoltNetwork.FrameDeltaTime * 4f;
            }

            state.XPosition = Mathf.Clamp(state.XPosition + deltaXPosition, -halfWidth, halfWidth);
        }

        void TouchStarted(UnityEngine.InputSystem.InputAction.CallbackContext context)
        {
            var touchPosition     = inputControls.Screen.Position.ReadValue<Vector2>();
            Vector2 touchWorldPos = Camera.main.ScreenToWorldPoint(new Vector3(touchPosition.x, touchPosition.y, 0f));
            lastTouchPosition     = touchWorldPos;
        }

        void TouchCanceled(UnityEngine.InputSystem.InputAction.CallbackContext context)
        {
            input.IsTouched   = false;
            lastTouchPosition = Vector2.zero;
        }
    }
}