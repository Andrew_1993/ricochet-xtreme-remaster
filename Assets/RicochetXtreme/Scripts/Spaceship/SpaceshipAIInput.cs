﻿using UnityEngine;

namespace RicochetXtremeRemaster
{
    public class SpaceshipAIInput : Bolt.EntityBehaviour<ISpaceship>
    {
        public override void SimulateOwner()
        {
            if (BallSpawner.Instance.Ball != null)
            {
                float speed = Random.Range(10f, 12f);
                float ballXPos = BallSpawner.Instance.Ball.transform.position.x;

                state.XPosition = Mathf.Lerp(state.XPosition, ballXPos, Time.deltaTime * speed);
            }
        }
    }
}