﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RicochetXtremeRemaster
{
    public class SpaceshipSpawner : MonoBehaviour
    {
        static SpaceshipSpawner instance;
        public static SpaceshipSpawner Instance
        {
            get
            {
                if (instance == null)
                {
                    var inst = FindObjectsOfType<SpaceshipSpawner>();

                    if (inst == null || inst.Length == 0)
                        throw new System.InvalidOperationException($"{typeof(SpaceshipSpawner).Name} not found!");

                    if (inst.Length > 1)
                        throw new System.InvalidOperationException($"{typeof(SpaceshipSpawner).Name} can be only one!");

                    instance = inst[0];
                }

                return instance;
            }
        }

        List<Spaceship> spaceships = new List<Spaceship>();
        public ReadOnlyCollection<Spaceship> Spaceships => spaceships.AsReadOnly();

        const int maxSpaceshipCount = 2;

        GameObject prefabSpaceshipAIInput;
        GameObject prefabSpaceshipDeviceInput;
        GameObject prefabSpaceshipNetworkInput;

        void Awake()
        {
            prefabSpaceshipAIInput      = Resources.Load<GameObject>("Prefabs/SpaceshipAIInput");
            prefabSpaceshipDeviceInput  = Resources.Load<GameObject>("Prefabs/SpaceshipDeviceInput");
            prefabSpaceshipNetworkInput = Resources.Load<GameObject>("Prefabs/SpaceshipNetworkInput");
        }

        public void SpawneSpaceship(bool isOwnerSpaceship)
        {
            if (spaceships.Count != maxSpaceshipCount)
            {
                BoltEntity entity;

                if (BoltNetwork.IsSinglePlayer)
                {
                    if (isOwnerSpaceship)
                        entity = BoltNetwork.Instantiate(prefabSpaceshipDeviceInput);
                    else
                        entity = BoltNetwork.Instantiate(prefabSpaceshipAIInput);
                }
                else
                {
                    if (isOwnerSpaceship)
                        entity = BoltNetwork.Instantiate(prefabSpaceshipDeviceInput);
                    else
                        entity = BoltNetwork.Instantiate(prefabSpaceshipNetworkInput);
                }

                var spaceship = entity.gameObject.GetComponent<Spaceship>();
                spaceships.Add(spaceship);

                spaceship.Initialize(isOwnerSpaceship);
            }
        }

        public void DestroySpaceships()
        {
            foreach (var spaceship in Spaceships)
            {
                Destroy(spaceship.gameObject);
            }

            spaceships.Clear();
        }
    }
}
