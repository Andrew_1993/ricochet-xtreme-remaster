﻿using RicochetXtremeRemaster.Multiplayer;
using UnityEngine;

namespace RicochetXtremeRemaster
{
    public class Spaceship : Bolt.EntityBehaviour<ISpaceship>
    {
        public bool IsOwner { get; private set; }

        void Update()
        {
            transform.position = new Vector2(state.XPosition, transform.position.y);
        }

        void SetupSkin()
        {
            var spaceshipBase = Utils.Find<SpriteRenderer>("Base", transform);

            if (BoltNetwork.IsSinglePlayer)
            {
                SaveLoadManager.LoadPlayerData(out var playerData);

                if (IsOwner)
                {
                    SpaceshipSkinManager.SetupSkin(spaceshipBase, playerData.SkinIndex);
                }
                else
                {
                    SpaceshipSkinManager.SetupSkin(spaceshipBase, 0);
                }
            }
            else
            {
                if (entity.IsOwner)
                {
                    SaveLoadManager.LoadPlayerData(out var playerData);
                    SpaceshipSkinManager.SetupSkin(spaceshipBase, playerData.SkinIndex);

                    NetworkEventListener.Instance.OnEntityAttachedAction += (entity) =>
                    {
                        var evnt = SetupSkinEvent.Create(Bolt.GlobalTargets.Everyone);
                        evnt.SkinIndex = playerData.SkinIndex;
                        evnt.Send();
                    };
                }
                else
                {
                    NetworkEventListener.Instance.OnClientSetupSkinAction += (skinIndex) =>
                    {
                        SpaceshipSkinManager.SetupSkin(spaceshipBase, skinIndex);
                    };
                }
            }
        }

        public void Initialize(bool isOwnerSpaceship)
        {
            IsOwner = isOwnerSpaceship;

            var gameAreaScale = GameArea.Instance.Scale;
            var gameAreaPosition = GameArea.Instance.Position;

            var yPosition = IsOwner ? (gameAreaPosition.y - (gameAreaScale.y / 2)) + .15f :
                                      (gameAreaPosition.y + (gameAreaScale.y / 2)) - .15f;
            var zRotation = IsOwner ? 0f : 180f;

            transform.position    = new Vector3(gameAreaPosition.x, yPosition, 0f);
            transform.eulerAngles = new Vector3(0f, 0f, zRotation);

            SetupSkin();
        }
    }
}