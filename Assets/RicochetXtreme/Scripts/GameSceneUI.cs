﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace RicochetXtremeRemaster
{
    public class GameSceneUI : MonoBehaviour
    {
        static GameSceneUI instance;
        public static GameSceneUI Instance
        {
            get
            {
                if (instance == null)
                {
                    var inst = FindObjectsOfType<GameSceneUI>();

                    if (inst == null || inst.Length == 0)
                        throw new InvalidOperationException($"{typeof(GameSceneUI).Name} not found!");

                    if (inst.Length > 1)
                        throw new InvalidOperationException($"{typeof(GameSceneUI).Name} can be only one!");

                    instance = inst[0];
                }

                return instance;
            }
        }

        public enum ViewType
        {
            None,
            GameMenu,
            GameMenuWaitingPlayer,
            GameMenuPaused,
            GameMenuStartingGame,
            GameMenuRespawningBall,
        }

        ViewType view = ViewType.None;
        public ViewType View
        {
            get => view;
            set
            {
                if (view == value)
                    return;

                UpdateView(show: false);

                view = value;

                UpdateView(show: true);
            }
        }

        // => Game menu
        public Button ButtonContinue { get; private set; }
        public Button ButtonExitToMenu { get; private set; }
        public Text TextTiming { get; private set; }
        public Text TextPlayerWaiting { get; private set; }
        public Text TextEnemyScore { get; private set; }
        public Text TextYourScore { get; private set; }

        void Awake()
        {
            ButtonContinue = Utils.Find<Button>("CanvasGameUI/ButtonContinue");
            ButtonExitToMenu = Utils.Find<Button>("CanvasGameUI/ButtonExitToMenu");

            TextTiming = Utils.Find<Text>("CanvasGameUI/TextTiming");
            TextPlayerWaiting = Utils.Find<Text>("CanvasGameUI/TextPlayerWaiting");
            TextEnemyScore = Utils.Find<Text>("CanvasGameUI/TextEnemyScore");
            TextYourScore = Utils.Find<Text>("CanvasGameUI/TextYourScore");

            View = ViewType.GameMenuWaitingPlayer;
        }

        void Update()
        {
            TextTiming.text     = GameSceneManager.Instance.CurrentSeconds.ToString();
            TextYourScore.text  = GameSceneManager.Instance.YourScore.ToString();
            TextEnemyScore.text = GameSceneManager.Instance.EnemyScore.ToString();
        }

        void UpdateView(bool show)
        {
            var fadeEnd = show ? 1f : 0f;

            switch (View)
            {
                case ViewType.None:
                    break;

                case ViewType.GameMenu:
                    TextEnemyScore.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    TextYourScore.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    break;

                case ViewType.GameMenuWaitingPlayer:
                    TextPlayerWaiting.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    break;

                case ViewType.GameMenuPaused:
                    ButtonContinue.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    ButtonContinue.GetComponent<CanvasGroup>().interactable = show;
                    ButtonContinue.GetComponent<CanvasGroup>().blocksRaycasts = show;

                    ButtonExitToMenu.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    ButtonExitToMenu.GetComponent<CanvasGroup>().interactable = show;
                    ButtonExitToMenu.GetComponent<CanvasGroup>().blocksRaycasts = show;
                    break;

                case ViewType.GameMenuStartingGame:
                    TextTiming.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    break;

                case ViewType.GameMenuRespawningBall:
                    TextTiming.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    TextEnemyScore.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    TextYourScore.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    break;
            }
        }
    }
}