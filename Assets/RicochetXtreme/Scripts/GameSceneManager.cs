﻿using RicochetXtremeRemaster.Multiplayer;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RicochetXtremeRemaster
{
    public class GameSceneManager : MonoBehaviour
    {
        public enum GameType
        {
            Singleplayer,
            Multiplayer,
        }

        static GameSceneManager instance;
        public static GameSceneManager Instance
        {
            get
            {
                if (instance == null)
                {
                    var inst = FindObjectsOfType<GameSceneManager>();

                    if (inst == null || inst.Length == 0)
                        throw new System.InvalidOperationException($"{typeof(GameSceneManager).Name} not found!");

                    if (inst.Length > 1)
                        throw new System.InvalidOperationException($"{typeof(GameSceneManager).Name} can be only one!");

                    instance = inst[0];
                }

                return instance;
            }
        }

        public GameType Type { get; private set; } = GameType.Singleplayer;
        public bool OwnerSessionIsPaused { get; private set; } = false;
        public bool ClientSessionIsPaused { get; private set; } = true;
        public float TimeBeforeStartGame { get; private set; } = 5f;
        public int CurrentSeconds { get; private set; }
        public int YourScore { get; private set; }
        public int EnemyScore { get; private set; }

        void Start()
        {
            CurrentSeconds = (int)TimeBeforeStartGame;

            GameSceneUI.Instance.ButtonContinue.onClick.AddListener(() =>
            {
                SendOnPauseEvent(false);
                ContinueGame();
            });

            GameSceneUI.Instance.ButtonExitToMenu.onClick.AddListener(() => ExitToMenu());

            if (BoltNetwork.IsSinglePlayer)
            {
                SpaceshipSpawner.Instance.SpawneSpaceship(true);
                SpaceshipSpawner.Instance.SpawneSpaceship(false);

                StartNewGame();
            }
            else
            {
                NetworkEventListener.Instance.OnEntityAttachedAction += (entity) =>
                {
                    if (!entity.IsOwner)
                    {
                        if (entity.TryGetComponent<Spaceship>(out var spaceship))
                        {
                            spaceship.Initialize(false);
                        }
                    }
                };

                NetworkEventListener.Instance.SceneLoadLocalDoneAction  += () => SpaceshipSpawner.Instance.SpawneSpaceship(true);

                NetworkEventListener.Instance.SceneLoadRemoteDoneAction += () => StartNewGame();

                NetworkEventListener.Instance.OnClientPauseAction += (isPaused) =>
                {
                    ClientSessionIsPaused = isPaused;

                    if (ClientSessionIsPaused)
                    {
                        if (!OwnerSessionIsPaused)
                        {
                            WaitingPlayer();
                        }
                    }
                    else
                    {
                        if (!OwnerSessionIsPaused)
                        {
                            ContinueGame();
                        }
                    }
                };
            }

            if (BoltNetwork.IsServer)
            {
                BallSpawner.Instance.SpawnBall();
            }

            NetworkEventListener.Instance.OnGoalAction += (serverScore, clientScore) => Goal(serverScore, clientScore);

            NetworkEventListener.Instance.OnDisconnectedAction += () => ExitToMenu();

            SoundManager.Instance.PlaySound(soundName: "GameMusic", volume: .3f, loopSound: true, destroyAfterComplete: false);
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                ButtonBackPressed();

            Timer();
        }

        void StartNewGame()
        {
            TimeBeforeStartGame   = 5f;
            OwnerSessionIsPaused  = false;
            ClientSessionIsPaused = false;

            GameSceneUI.Instance.View = GameSceneUI.ViewType.GameMenuStartingGame;
        }

        void PauseGame()
        {
            OwnerSessionIsPaused = true;

            GameSceneUI.Instance.View = GameSceneUI.ViewType.GameMenuPaused;
            SoundManager.Instance.PauseSound("GameMusic");
            SoundManager.Instance.PlaySound(soundName: "Pause", loopSound: false, destroyAfterComplete: true);
        }

        void ContinueGame()
        {
            OwnerSessionIsPaused = false;

            if (ClientSessionIsPaused)
            {
                WaitingPlayer();
            }
            else
            {
                TimeBeforeStartGame = 5f;

                GameSceneUI.Instance.View = GameSceneUI.ViewType.GameMenuStartingGame;
                SoundManager.Instance.UnPauseSound("GameMusic");
                SoundManager.Instance.PlaySound(soundName: "ButtonClick", loopSound: false, destroyAfterComplete: true);
            }
        }

        void ExitToMenu()
        {
            SaveLoadManager.SaveBestScore(YourScore);

            BoltLauncher.Shutdown();

            foreach (var connection in BoltNetwork.Connections)
            {
                connection.Disconnect();
            }

            foreach (var entity in BoltNetwork.Entities)
            {
                BoltNetwork.Destroy(entity.gameObject);
            }

            SceneManager.LoadScene("MainMenu");
        }

        void WaitingPlayer()
        {
            GameSceneUI.Instance.View = GameSceneUI.ViewType.GameMenuWaitingPlayer;
        }

        void ButtonBackPressed()
        {
            if (OwnerSessionIsPaused)
            {
                SendOnPauseEvent(false);
                ContinueGame();
            }
            else
            {
                SendOnPauseEvent(true);
                PauseGame();
            }
        }

        void SendOnPauseEvent(bool isPaused)
        {
            var pauseEvent = PauseEvent.Create(Bolt.GlobalTargets.Everyone);
            pauseEvent.IsPaused = isPaused;
            pauseEvent.Send();
        }

        void Timer()
        {
            if (!OwnerSessionIsPaused && !ClientSessionIsPaused)
            {
                TimeBeforeStartGame -= Time.deltaTime;
                if (TimeBeforeStartGame <= 0)
                {
                    TimeBeforeStartGame = 0f;
                    GameSceneUI.Instance.View = GameSceneUI.ViewType.GameMenu;
                }

                var seconds = (int)System.Math.Round(TimeBeforeStartGame, 0);

                if (CurrentSeconds != seconds)
                    SoundManager.Instance.PlaySound(soundName: "Timer", loopSound: false, destroyAfterComplete: true);

                CurrentSeconds = seconds;
            }
        }

        void Goal(int serverScore, int clientScore)
        {
            if (BoltNetwork.IsServer)
            {
                YourScore = serverScore;
                EnemyScore = clientScore;

                BallSpawner.Instance.RespawneBall();
            }
            else
            {
                YourScore = clientScore;
                EnemyScore = serverScore;
            }

            TimeBeforeStartGame = 2f;

            GameSceneUI.Instance.View = GameSceneUI.ViewType.GameMenuRespawningBall;

            SoundManager.Instance.PlaySound(soundName: "Goal", loopSound: false, destroyAfterComplete: true);
        }
    }
}
