﻿using UnityEngine;

public static class Utils
{
    public static T Find<T>(string path, Transform root = null) where T : UnityEngine.Object
    {
        if (string.IsNullOrEmpty(path))
        {
            Debug.LogError("Path is empty", root);
            return null;
        }

        GameObject go;
        if (root != null)
        {
            var transform = root.Find(path);
            go = transform != null ? transform.gameObject : null;
        }
        else
        {
            go = GameObject.Find("/" + path);
        }

        if (go == null)
        {
            Debug.LogError($"Object not found \"{path}\"", root);
            return null;
        }

        return go is T ? go as T : go.GetComponent<T>();
    }
    public static float Remap(float num, float low1, float high1, float low2, float high2)
    {
        return Mathf.Clamp(low2 + (num - low1) * (high2 - low2) / (high1 - low1), Mathf.Min(low2, high2), Mathf.Max(low2, high2));
    }
}
