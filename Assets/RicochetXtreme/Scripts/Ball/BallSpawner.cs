﻿using UnityEngine;

namespace RicochetXtremeRemaster
{
    public class BallSpawner : MonoBehaviour
    {
        static BallSpawner instance;
        public static BallSpawner Instance
        {
            get
            {
                if (instance == null)
                {
                    var inst = FindObjectsOfType<BallSpawner>();

                    if (inst == null || inst.Length == 0)
                        throw new System.InvalidOperationException($"{typeof(BallSpawner).Name} not found!");

                    if (inst.Length > 1)
                        throw new System.InvalidOperationException($"{typeof(BallSpawner).Name} can be only one!");

                    instance = inst[0];
                }

                return instance;
            }
        }

        public Ball Ball { get; private set; }

        public void SpawnBall()
        {
            var entity = BoltNetwork.Instantiate(BoltPrefabs.BallSimulationModel, Vector3.zero, Quaternion.identity);
            Ball = entity.gameObject.GetComponent<Ball>();
        }

        public void RespawneBall()
        {
            Ball.GetComponent<BoltEntity>().DestroyDelayed(0);

            SpawnBall();
        }
    }
}