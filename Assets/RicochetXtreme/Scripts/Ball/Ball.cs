﻿using UnityEngine;

namespace RicochetXtremeRemaster
{
    public class Ball : Bolt.EntityBehaviour<IBall>
    {
        Rigidbody2D ballRigidbody;
        Transform ballRenderModel;
        Vector2 direction;
        int speed;
        bool inGate;

        int delayFrameCount = 12;
        int delayInFrames;

        public override void Attached()
        {
            ballRigidbody = GetComponent<Rigidbody2D>();

            if (entity.IsOwner)
            {
                speed = Random.Range(2, 3);
                direction = new Vector2(0f, Random.Range(-1f, 1f) <= 0 ? -1 : 1);
            }
            else
            {
                Destroy(ballRigidbody);
                Destroy(GetComponent<Collider2D>());
            }

            var renderModel = Resources.Load<Transform>("Prefabs/BallRenderModel");
            ballRenderModel = Instantiate(renderModel);

            state.SetTransforms(state.Transform, transform);
        }

        public override void Detached()
        {
            Destroy(ballRenderModel.gameObject);
        }

        public override void SimulateOwner()
        {
            if (!GameSceneManager.Instance.OwnerSessionIsPaused && !GameSceneManager.Instance.ClientSessionIsPaused && GameSceneManager.Instance.TimeBeforeStartGame <= 0f)
                ballRigidbody.velocity = direction.normalized * (float)speed;
            else
                ballRigidbody.velocity = Vector2.zero;

            ballRenderModel.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);

            if (BoltNetwork.IsServer)
                CheckBallPosition();
        }

        void Update()
        {
            // Если позиция шара обновляется в мультиплеере у клиента, то инвертируем Y позицию
            if (!entity.IsOwner)
            {
                ballRenderModel.position = new Vector3(transform.position.x, transform.position.y * -1, transform.position.z);
            }

            ballRenderModel.Rotate(new Vector3(0f, 0f, .5f));

            --delayInFrames;
            delayInFrames = Mathf.Clamp(delayInFrames, 0, delayFrameCount);
        }

        void OnCollisionEnter2D(Collision2D collision)
        {
            // Иногда шар может удариться о карабль много раз подряд, что приводит к непонятным эффектам,
            // поэтому делаем небольшую задержку перед следующей коллизией.
            if (delayInFrames > 0)
                return;

            delayInFrames = delayFrameCount;

            direction = Vector3.Reflect(direction, collision.contacts[0].normal);

            if (Mathf.Abs(direction.y) < .5f)
            {
                var remap = Utils.Remap(direction.y, .5f, 0f, 0f, 1f);
                var boost = Mathf.Lerp(.1f, .5f, remap);

                direction.y = Mathf.Sign(direction.y) * boost;
            }

            SoundManager.Instance.PlaySound(soundName: "Bounce", loopSound: false, destroyAfterComplete: true);
        }

        void CheckBallPosition()
        {
            if (inGate)
                return;

            float upEdge = GameArea.Instance.Scale.y / 2f;
            float downEdge = -GameArea.Instance.Scale.y / 2f;
            float ballYPos = transform.position.y;

            if (ballYPos <= downEdge) // In your gates scored
            {
                var goalEvent = GoalEvent.Create();
                goalEvent.ServerScore = GameSceneManager.Instance.YourScore;
                goalEvent.ClientScore = GameSceneManager.Instance.EnemyScore + 1;
                goalEvent.Send();

                inGate = true;
            }
            else if (ballYPos >= upEdge) // You Score goal
            {
                var goalEvent = GoalEvent.Create();
                goalEvent.ServerScore = GameSceneManager.Instance.YourScore + 1;
                goalEvent.ClientScore = GameSceneManager.Instance.EnemyScore;
                goalEvent.Send();

                inGate = true;
            }
        }
    }
}