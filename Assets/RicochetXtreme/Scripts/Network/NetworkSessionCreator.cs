﻿using Bolt;
using Bolt.Matchmaking;
using System;
using UdpKit;

public class NetworkSessionCreator : GlobalEventListener
{
    static NetworkSessionCreator instance;
    public static NetworkSessionCreator Instance
    {
        get
        {
            if (instance == null)
            {
                var inst = FindObjectsOfType<NetworkSessionCreator>();

                if (inst == null || inst.Length == 0)
                    throw new InvalidOperationException($"{typeof(NetworkSessionCreator).Name} not found!");

                if (inst.Length > 1)
                    throw new InvalidOperationException($"{typeof(NetworkSessionCreator).Name} can be only one!");

                instance = inst[0];
            }

            return instance;
        }
    }

    public override void BoltStartDone()
    {
        if (BoltNetwork.IsServer)
        {
            string sessionID = "RicochetXtremeMatch";

            BoltMatchmaking.CreateSession(sessionID, sceneToLoad: "Game");
        }
    }

    public override void SessionListUpdated(Map<Guid, UdpSession> sessionList)
    {
        foreach (var session in sessionList)
        {
            UdpSession photoneSession = session.Value as UdpSession;

            if (photoneSession.Source == UdpSessionSource.Photon)
            {
                BoltNetwork.Connect(photoneSession);
            }
        }
    }
}
