﻿using Bolt;
using System;

namespace RicochetXtremeRemaster.Multiplayer
{
    [BoltGlobalBehaviour]
    public class NetworkEventListener : GlobalEventListener
    {
        static NetworkEventListener instance;
        public static NetworkEventListener Instance
        {
            get
            {
                if (instance == null)
                {
                    var inst = FindObjectsOfType<NetworkEventListener>();

                    if (inst == null || inst.Length == 0)
                        throw new InvalidOperationException($"{typeof(NetworkEventListener).Name} not found!");

                    if (inst.Length > 1)
                        throw new InvalidOperationException($"{typeof(NetworkEventListener).Name} can be only one!");

                    instance = inst[0];
                }

                return instance;
            }
        }

        public event Action SceneLoadLocalDoneAction;
        public event Action SceneLoadRemoteDoneAction;
        public event Action<BoltEntity> OnEntityAttachedAction;
        public event Action<bool> OnClientPauseAction;
        public event Action<int, int> OnGoalAction;
        public event Action OnDisconnectedAction;
        public event Action<int> OnClientSetupSkinAction;

        public override void SceneLoadLocalDone(string scene)
        {
            SceneLoadLocalDoneAction?.Invoke();
        }

        public override void SceneLoadRemoteDone(BoltConnection connection)
        {
            SceneLoadRemoteDoneAction?.Invoke();
        }

        public override void EntityAttached(BoltEntity entity)
        {
            OnEntityAttachedAction?.Invoke(entity);
        }

        public override void OnEvent(PauseEvent evnt)
        {
            if (!evnt.FromSelf)
                OnClientPauseAction?.Invoke(evnt.IsPaused);
        }

        public override void OnEvent(GoalEvent evnt)
        {
            OnGoalAction?.Invoke(evnt.ServerScore, evnt.ClientScore);
        }

        public override void OnEvent(SetupSkinEvent evnt)
        {
            if (!evnt.FromSelf)
                OnClientSetupSkinAction?.Invoke(evnt.SkinIndex);
        }

        public override void Disconnected(BoltConnection connection)
        {
            OnDisconnectedAction?.Invoke();
        }
    }
}