﻿using UnityEngine;

namespace RicochetXtremeRemaster
{
    public class GameArea : MonoBehaviour
    {
        static GameArea instance;
        public static GameArea Instance
        {
            get
            {
                if (instance == null)
                {
                    var inst = FindObjectsOfType<GameArea>();

                    if (inst == null || inst.Length == 0)
                        throw new System.InvalidOperationException($"{typeof(GameArea).Name} not found!");

                    if (inst.Length > 1)
                        throw new System.InvalidOperationException($"{typeof(GameArea).Name} can be only one!");

                    instance = inst[0];
                }

                return instance;
            }
        }

        public Vector2 Scale { get => transform.lossyScale; }
        public Vector2 Position { get => transform.position; }
    }
}