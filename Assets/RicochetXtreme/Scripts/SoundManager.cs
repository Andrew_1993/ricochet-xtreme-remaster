﻿using System.Collections.Generic;
using UnityEngine;

namespace RicochetXtremeRemaster
{
    public class SoundManager : MonoBehaviour
    {
        static SoundManager instance;
        public static SoundManager Instance
        {
            get
            {
                if (instance == null)
                {
                    var inst = FindObjectsOfType<SoundManager>();

                    if (inst == null || inst.Length == 0)
                        throw new System.InvalidOperationException($"{typeof(SoundManager).Name} not found!");

                    if (inst.Length > 1)
                        throw new System.InvalidOperationException($"{typeof(SoundManager).Name} can be only one!");

                    instance = inst[0];
                }

                return instance;
            }
        }

        AudioClip[] audioClips;
        List<AudioSource> playingSounds = new List<AudioSource>();

        public void PlaySound(string soundName, bool loopSound, bool destroyAfterComplete, float volume = 1.0f)
        {
            playingSounds.RemoveAll(source => source == null);

            if (GetAudioClip(soundName, out var audioClip))
            {
                var newGameObject = new GameObject(soundName);
                var newAudioSource = newGameObject.AddComponent<AudioSource>();

                newAudioSource.volume = volume;
                newAudioSource.clip = audioClip;
                newAudioSource.loop = loopSound;
                newAudioSource.Play();

                playingSounds.Add(newAudioSource);

                if (destroyAfterComplete)
                    Destroy(newGameObject, audioClip.length);
            }
        }

        public void PauseSound(string soundName)
        {
            playingSounds.RemoveAll(source => source == null);

            foreach (var item in playingSounds)
            {
                if (item.name == soundName)
                    item.Pause();
            }
        }

        public void UnPauseSound(string soundName)
        {
            playingSounds.RemoveAll(source => source == null);

            foreach (var item in playingSounds)
            {
                if (item.name == soundName)
                    item.UnPause();
            }
        }

        bool GetAudioClip(string soundName, out AudioClip audioClip)
        {
            audioClip = null;

            if (audioClips == null || audioClips.Length == 0)
                audioClips = Resources.LoadAll<AudioClip>("SoundFX");

            if (audioClips != null && audioClips.Length > 0)
            {
                foreach (var clip in audioClips)
                {
                    if (clip.name == soundName)
                    {
                        audioClip = clip;
                        return true;
                    }
                }
            }

            Debug.LogWarning($"Sound file {soundName} not found!");

            return false;
        }
    }
}
