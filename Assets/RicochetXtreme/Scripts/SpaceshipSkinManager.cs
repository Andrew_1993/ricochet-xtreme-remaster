﻿using UnityEngine;
using UnityEngine.UI;

public static class SpaceshipSkinManager
{
    public static int SkinCount => LoadSpritesFromResources() ? sprites.Length : 0;

    static Sprite[] sprites;

    public static void SetupSkin(SpriteRenderer target, int skinIndex)
    {
        if (LoadSpritesFromResources())
        {
            target.sprite = sprites[skinIndex];
        }
    }

    public static void SetupSkin(Image target, int skinIndex)
    {
        if (LoadSpritesFromResources())
        {
            target.sprite = sprites[skinIndex];
        }
    }

    static bool LoadSpritesFromResources()
    {
        if (sprites == null)
        {
            sprites = Resources.LoadAll<Sprite>("SpaceshipSkins");
        }

        if (sprites == null || sprites.Length == 0)
        {
            Debug.LogError("Spaceship skins not found!");

            return false;
        }

        return true;
    }
}
