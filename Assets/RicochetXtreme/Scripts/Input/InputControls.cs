// GENERATED AUTOMATICALLY FROM 'Assets/RicochetXtreme/Scripts/Input/InputControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace RicochetXtremeRemaster
{
    public class @InputControls : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @InputControls()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputControls"",
    ""maps"": [
        {
            ""name"": ""Screen"",
            ""id"": ""2b54dcf9-b0a9-4d17-8651-3380a60d2ec6"",
            ""actions"": [
                {
                    ""name"": ""Touch"",
                    ""type"": ""Button"",
                    ""id"": ""3b03eed9-ec19-454f-ad9d-a2ec43021cd9"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Position"",
                    ""type"": ""Value"",
                    ""id"": ""0decb11c-1ae6-41b9-9f14-8ee568a32d13"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""e70febc1-2a11-4743-805a-7ae6621806e7"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Touch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""33c55bb7-2574-4598-b8ee-b4153df58915"",
                    ""path"": ""<Touchscreen>/press"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Touch"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dfdf35f8-2d59-444f-8d28-9c0d46eb4cf3"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Position"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e3a061e4-d698-4e31-917e-44717f061682"",
                    ""path"": ""<Touchscreen>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Position"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Keyboard"",
            ""id"": ""59cf90af-b046-4554-9cde-5d7eb55f9216"",
            ""actions"": [
                {
                    ""name"": ""LeftRightArrows"",
                    ""type"": ""Value"",
                    ""id"": ""366359c5-017f-45c1-ab97-c7e7517adb1d"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""Arrows"",
                    ""id"": ""02c62403-0e76-4cf2-90bf-756fe2847d18"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftRightArrows"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""7c017687-ee47-435b-8970-19e29e37c302"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftRightArrows"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""1f67ff84-9186-46e0-ac21-bb0bf09f1490"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftRightArrows"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        },
        {
            ""name"": ""Gamepad"",
            ""id"": ""fe597598-842a-4b4e-b5e6-7c4c0c4a207f"",
            ""actions"": [
                {
                    ""name"": ""LeftRightSticks"",
                    ""type"": ""Value"",
                    ""id"": ""fa8eadd6-6bbb-467c-a371-c3872081bae9"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""87fef48e-4df0-427e-b3ab-ee8275f29c19"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftRightSticks"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a2e19f93-95ee-44aa-92ea-4d882a5558b7"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftRightSticks"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
            // Screen
            m_Screen = asset.FindActionMap("Screen", throwIfNotFound: true);
            m_Screen_Touch = m_Screen.FindAction("Touch", throwIfNotFound: true);
            m_Screen_Position = m_Screen.FindAction("Position", throwIfNotFound: true);
            // Keyboard
            m_Keyboard = asset.FindActionMap("Keyboard", throwIfNotFound: true);
            m_Keyboard_LeftRightArrows = m_Keyboard.FindAction("LeftRightArrows", throwIfNotFound: true);
            // Gamepad
            m_Gamepad = asset.FindActionMap("Gamepad", throwIfNotFound: true);
            m_Gamepad_LeftRightSticks = m_Gamepad.FindAction("LeftRightSticks", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Screen
        private readonly InputActionMap m_Screen;
        private IScreenActions m_ScreenActionsCallbackInterface;
        private readonly InputAction m_Screen_Touch;
        private readonly InputAction m_Screen_Position;
        public struct ScreenActions
        {
            private @InputControls m_Wrapper;
            public ScreenActions(@InputControls wrapper) { m_Wrapper = wrapper; }
            public InputAction @Touch => m_Wrapper.m_Screen_Touch;
            public InputAction @Position => m_Wrapper.m_Screen_Position;
            public InputActionMap Get() { return m_Wrapper.m_Screen; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(ScreenActions set) { return set.Get(); }
            public void SetCallbacks(IScreenActions instance)
            {
                if (m_Wrapper.m_ScreenActionsCallbackInterface != null)
                {
                    @Touch.started -= m_Wrapper.m_ScreenActionsCallbackInterface.OnTouch;
                    @Touch.performed -= m_Wrapper.m_ScreenActionsCallbackInterface.OnTouch;
                    @Touch.canceled -= m_Wrapper.m_ScreenActionsCallbackInterface.OnTouch;
                    @Position.started -= m_Wrapper.m_ScreenActionsCallbackInterface.OnPosition;
                    @Position.performed -= m_Wrapper.m_ScreenActionsCallbackInterface.OnPosition;
                    @Position.canceled -= m_Wrapper.m_ScreenActionsCallbackInterface.OnPosition;
                }
                m_Wrapper.m_ScreenActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Touch.started += instance.OnTouch;
                    @Touch.performed += instance.OnTouch;
                    @Touch.canceled += instance.OnTouch;
                    @Position.started += instance.OnPosition;
                    @Position.performed += instance.OnPosition;
                    @Position.canceled += instance.OnPosition;
                }
            }
        }
        public ScreenActions @Screen => new ScreenActions(this);

        // Keyboard
        private readonly InputActionMap m_Keyboard;
        private IKeyboardActions m_KeyboardActionsCallbackInterface;
        private readonly InputAction m_Keyboard_LeftRightArrows;
        public struct KeyboardActions
        {
            private @InputControls m_Wrapper;
            public KeyboardActions(@InputControls wrapper) { m_Wrapper = wrapper; }
            public InputAction @LeftRightArrows => m_Wrapper.m_Keyboard_LeftRightArrows;
            public InputActionMap Get() { return m_Wrapper.m_Keyboard; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(KeyboardActions set) { return set.Get(); }
            public void SetCallbacks(IKeyboardActions instance)
            {
                if (m_Wrapper.m_KeyboardActionsCallbackInterface != null)
                {
                    @LeftRightArrows.started -= m_Wrapper.m_KeyboardActionsCallbackInterface.OnLeftRightArrows;
                    @LeftRightArrows.performed -= m_Wrapper.m_KeyboardActionsCallbackInterface.OnLeftRightArrows;
                    @LeftRightArrows.canceled -= m_Wrapper.m_KeyboardActionsCallbackInterface.OnLeftRightArrows;
                }
                m_Wrapper.m_KeyboardActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @LeftRightArrows.started += instance.OnLeftRightArrows;
                    @LeftRightArrows.performed += instance.OnLeftRightArrows;
                    @LeftRightArrows.canceled += instance.OnLeftRightArrows;
                }
            }
        }
        public KeyboardActions @Keyboard => new KeyboardActions(this);

        // Gamepad
        private readonly InputActionMap m_Gamepad;
        private IGamepadActions m_GamepadActionsCallbackInterface;
        private readonly InputAction m_Gamepad_LeftRightSticks;
        public struct GamepadActions
        {
            private @InputControls m_Wrapper;
            public GamepadActions(@InputControls wrapper) { m_Wrapper = wrapper; }
            public InputAction @LeftRightSticks => m_Wrapper.m_Gamepad_LeftRightSticks;
            public InputActionMap Get() { return m_Wrapper.m_Gamepad; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(GamepadActions set) { return set.Get(); }
            public void SetCallbacks(IGamepadActions instance)
            {
                if (m_Wrapper.m_GamepadActionsCallbackInterface != null)
                {
                    @LeftRightSticks.started -= m_Wrapper.m_GamepadActionsCallbackInterface.OnLeftRightSticks;
                    @LeftRightSticks.performed -= m_Wrapper.m_GamepadActionsCallbackInterface.OnLeftRightSticks;
                    @LeftRightSticks.canceled -= m_Wrapper.m_GamepadActionsCallbackInterface.OnLeftRightSticks;
                }
                m_Wrapper.m_GamepadActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @LeftRightSticks.started += instance.OnLeftRightSticks;
                    @LeftRightSticks.performed += instance.OnLeftRightSticks;
                    @LeftRightSticks.canceled += instance.OnLeftRightSticks;
                }
            }
        }
        public GamepadActions @Gamepad => new GamepadActions(this);
        public interface IScreenActions
        {
            void OnTouch(InputAction.CallbackContext context);
            void OnPosition(InputAction.CallbackContext context);
        }
        public interface IKeyboardActions
        {
            void OnLeftRightArrows(InputAction.CallbackContext context);
        }
        public interface IGamepadActions
        {
            void OnLeftRightSticks(InputAction.CallbackContext context);
        }
    }
}
