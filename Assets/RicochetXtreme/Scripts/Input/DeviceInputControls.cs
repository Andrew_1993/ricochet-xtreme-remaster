// GENERATED AUTOMATICALLY FROM 'Assets/RicochetXtreme/Scripts/Input/InputControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace RicochetXtremeRemaster
{
    public class DeviceInputControls : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public DeviceInputControls()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputControls"",
    ""maps"": [
        {
            ""name"": ""Spaceship"",
            ""id"": ""59cf90af-b046-4554-9cde-5d7eb55f9216"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""Value"",
                    ""id"": ""366359c5-017f-45c1-ab97-c7e7517adb1d"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""6bbd7e4b-65d4-4912-86bc-2c959f07b4f2"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""64971e4e-2dbe-4dbe-b21f-49fe59eb2f2f"",
                    ""path"": ""<Gamepad>/leftStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Menu"",
            ""id"": ""7c27fff1-b802-4982-b16e-3b73dc9c81e0"",
            ""actions"": [],
            ""bindings"": []
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Gamepad"",
            ""bindingGroup"": ""Gamepad"",
            ""devices"": [
                {
                    ""devicePath"": ""<Gamepad>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        },
        {
            ""name"": ""Keyboard$Mouse"",
            ""bindingGroup"": ""Keyboard$Mouse"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                },
                {
                    ""devicePath"": ""<Mouse>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
            // Spaceship
            m_Spaceship = asset.FindActionMap("Spaceship", throwIfNotFound: true);
            m_Spaceship_Move = m_Spaceship.FindAction("Move", throwIfNotFound: true);
            // Menu
            m_Menu = asset.FindActionMap("Menu", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Spaceship
        private readonly InputActionMap m_Spaceship;
        private ISpaceshipActions m_SpaceshipActionsCallbackInterface;
        private readonly InputAction m_Spaceship_Move;
        public struct SpaceshipActions
        {
            private DeviceInputControls m_Wrapper;
            public SpaceshipActions(DeviceInputControls wrapper) { m_Wrapper = wrapper; }
            public InputAction @Move => m_Wrapper.m_Spaceship_Move;
            public InputActionMap Get() { return m_Wrapper.m_Spaceship; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(SpaceshipActions set) { return set.Get(); }
            public void SetCallbacks(ISpaceshipActions instance)
            {
                if (m_Wrapper.m_SpaceshipActionsCallbackInterface != null)
                {
                    @Move.started -= m_Wrapper.m_SpaceshipActionsCallbackInterface.OnMove;
                    @Move.performed -= m_Wrapper.m_SpaceshipActionsCallbackInterface.OnMove;
                    @Move.canceled -= m_Wrapper.m_SpaceshipActionsCallbackInterface.OnMove;
                }
                m_Wrapper.m_SpaceshipActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Move.started += instance.OnMove;
                    @Move.performed += instance.OnMove;
                    @Move.canceled += instance.OnMove;
                }
            }
        }
        public SpaceshipActions @Spaceship => new SpaceshipActions(this);

        // Menu
        private readonly InputActionMap m_Menu;
        private IMenuActions m_MenuActionsCallbackInterface;
        public struct MenuActions
        {
            private DeviceInputControls m_Wrapper;
            public MenuActions(DeviceInputControls wrapper) { m_Wrapper = wrapper; }
            public InputActionMap Get() { return m_Wrapper.m_Menu; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(MenuActions set) { return set.Get(); }
            public void SetCallbacks(IMenuActions instance)
            {
                if (m_Wrapper.m_MenuActionsCallbackInterface != null)
                {
                }
                m_Wrapper.m_MenuActionsCallbackInterface = instance;
                if (instance != null)
                {
                }
            }
        }
        public MenuActions @Menu => new MenuActions(this);
        private int m_GamepadSchemeIndex = -1;
        public InputControlScheme GamepadScheme
        {
            get
            {
                if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
                return asset.controlSchemes[m_GamepadSchemeIndex];
            }
        }
        private int m_KeyboardMouseSchemeIndex = -1;
        public InputControlScheme KeyboardMouseScheme
        {
            get
            {
                if (m_KeyboardMouseSchemeIndex == -1) m_KeyboardMouseSchemeIndex = asset.FindControlSchemeIndex("Keyboard$Mouse");
                return asset.controlSchemes[m_KeyboardMouseSchemeIndex];
            }
        }
        public interface ISpaceshipActions
        {
            void OnMove(InputAction.CallbackContext context);
        }
        public interface IMenuActions
        {
        }
    }
}
