﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace RicochetXtremeRemaster
{
    public class MainMenuSceneUI : MonoBehaviour
    {
        static MainMenuSceneUI instance;
        public static MainMenuSceneUI Instance
        {
            get
            {
                if (instance == null)
                {
                    var inst = FindObjectsOfType<MainMenuSceneUI>();

                    if (inst == null || inst.Length == 0)
                        throw new InvalidOperationException($"{typeof(MainMenuSceneUI).Name} not found!");

                    if (inst.Length > 1)
                        throw new InvalidOperationException($"{typeof(MainMenuSceneUI).Name} can be only one!");

                    instance = inst[0];
                }

                return instance;
            }
        }

        public enum ViewType
        {
            None,
            MainMenu,
            NetworkServerCreation,
            NetworkConnectionToServer,
            MenuChooseSkin,
        }

        ViewType view = ViewType.None;
        public ViewType View
        {
            get => view;
            set
            {
                if (view == value)
                    return;

                UpdateView(show: false);

                view = value;

                UpdateView(show: true);
            }
        }

        // => Main menu
        public Button ButtonSignleplayer    { get; private set; }
        public Button ButtonStartServer     { get; private set; }
        public Button ButtonConnectToServer { get; private set; }
        public Button ButtonChooseSkin      { get; private set; }
        public Text TextBestScore       { get; private set; }

        // => Choose skin menu
        public Button ButtonNextSkin     { get; private set; }
        public Button ButtonPreviousSkin { get; private set; }
        public Button ButtonSaveAndBack  { get; private set; }
        public GameObject SkinPreview    { get; private set; }
        public Image ImageSkin           { get; private set; }

        // => Network messages
        public Text TextServerCreation     { get; private set; }
        public Text TextConnectionToServer { get; private set; }

        void Awake()
        {
            ButtonSignleplayer    = Utils.Find<Button>("CanvasMainMenuUI/ButtonSignlePlayer");
            ButtonStartServer     = Utils.Find<Button>("CanvasMainMenuUI/ButtonStartServer");
            ButtonConnectToServer = Utils.Find<Button>("CanvasMainMenuUI/ButtonConnectToServer");
            ButtonChooseSkin      = Utils.Find<Button>("CanvasMainMenuUI/ButtonChooseSkin");
            TextBestScore         = Utils.Find<Text>("CanvasMainMenuUI/TextBestScore");

            ButtonNextSkin     = Utils.Find<Button>("CanvasMainMenuUI/ButtonNextSkin");
            ButtonPreviousSkin = Utils.Find<Button>("CanvasMainMenuUI/ButtonPreviousSkin");
            ButtonSaveAndBack  = Utils.Find<Button>("CanvasMainMenuUI/ButtonSaveAndBack");
            SkinPreview        = Utils.Find<GameObject>("CanvasMainMenuUI/SkinPreview");
            ImageSkin          = Utils.Find<Image>("CanvasMainMenuUI/SkinPreview/ImageSkin");

            TextServerCreation     = Utils.Find<Text>("CanvasMainMenuUI/TextServerCreation");
            TextConnectionToServer = Utils.Find<Text>("CanvasMainMenuUI/TextConnectionToServer");
        }

        void UpdateView(bool show, bool animation = true)
        {
            var fadeEnd  = show ? 1f : 0f;
            var duration = animation ? .3f : 0f;

            switch (View)
            {
                case ViewType.None:
                    break;

                case ViewType.MainMenu:
                    ButtonSignleplayer.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    ButtonSignleplayer.GetComponent<CanvasGroup>().interactable = show;
                    ButtonSignleplayer.GetComponent<CanvasGroup>().blocksRaycasts = show;

                    ButtonStartServer.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    ButtonStartServer.GetComponent<CanvasGroup>().interactable = show;
                    ButtonStartServer.GetComponent<CanvasGroup>().blocksRaycasts = show;

                    ButtonConnectToServer.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    ButtonConnectToServer.GetComponent<CanvasGroup>().interactable = show;
                    ButtonConnectToServer.GetComponent<CanvasGroup>().blocksRaycasts = show;

                    ButtonChooseSkin.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    ButtonChooseSkin.GetComponent<CanvasGroup>().interactable = show;
                    ButtonChooseSkin.GetComponent<CanvasGroup>().blocksRaycasts = show;

                    TextBestScore.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    break;

                case ViewType.NetworkServerCreation:
                    TextServerCreation.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    break;

                case ViewType.NetworkConnectionToServer:
                    TextConnectionToServer.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    break;

                case ViewType.MenuChooseSkin:
                    ButtonNextSkin.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    ButtonNextSkin.GetComponent<CanvasGroup>().interactable = show;
                    ButtonNextSkin.GetComponent<CanvasGroup>().blocksRaycasts = show;

                    ButtonPreviousSkin.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    ButtonPreviousSkin.GetComponent<CanvasGroup>().interactable = show;
                    ButtonPreviousSkin.GetComponent<CanvasGroup>().blocksRaycasts = show;

                    ButtonSaveAndBack.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    ButtonSaveAndBack.GetComponent<CanvasGroup>().interactable = show;
                    ButtonSaveAndBack.GetComponent<CanvasGroup>().blocksRaycasts = show;

                    SkinPreview.GetComponent<CanvasGroup>().alpha = fadeEnd;
                    break;

                default:
                    break;
            }
        }
    }
}