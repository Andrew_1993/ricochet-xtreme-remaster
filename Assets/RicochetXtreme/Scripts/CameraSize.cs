﻿using UnityEngine;

namespace RicochetXtremeRemaster
{
    public class CameraSize : MonoBehaviour
    {
        Transform gameArea;
        Camera cam;

        void Awake()
        {
            gameArea = Utils.Find<Transform>("GameArea");
            cam = GetComponent<Camera>();
        }

        void Update()
        {
            float screenRatio = (float)Screen.width / (float)Screen.height;
            float targetRatio = gameArea.lossyScale.x / gameArea.lossyScale.y;

            if (screenRatio >= targetRatio)
            {
                cam.orthographicSize = gameArea.lossyScale.y / 2;
            }
            else
            {
                float differenceInSize = targetRatio / screenRatio;
                cam.orthographicSize = gameArea.lossyScale.y / 2 * differenceInSize;
            }
        }
    }
}
